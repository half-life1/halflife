﻿
![picture](https://cdn2.steamgriddb.com/logo_thumb/82367463300c6d423d4bf86699b0d20b.png) 

Half Life setup/launch script powered by xash3d-fwgs. Note: you will require to own Half Life on Steam for it to work so the setup script can import the game files.

[Half Life Steam:](https://store.steampowered.com/app/70/HalfLife/))

The binaries were compiled from the official xash3d-fwgs repo
https://github.com/FWGS/xash3d-fwgs

This has no affiliation with Valve

Packages: 

[halflife-xash3d-fwgs](https://aur.archlinux.org/packages/halflife-xash3d-fwgs))

[xash3d-fwgs-bin](https://aur.archlinux.org/packages/xash3d-fwgs-bin))

 ### Author
  * Corey Bruce
